<?php
header('Content-Type: application/json');
$json = json_decode(file_get_contents('php://input'), true);
$endpoint    = "URL_API";
$output_file = "/pool/lexart_img_".rand();
$baseurl     = $endpoint.$output_file;

// EXAMPLE OBJECT
// POST / { "img_encoded": "base64_file", "img_type": "file_tipe (jpg, pdf, etc)" }

if(!empty($json["img_encoded"])){
  $type = $json["img_type"];
  $ifp 	= fopen( getcwd().$output_file."_.".$type, 'w' );
  $data = explode( ',', $json["img_encoded"] );

  fwrite( $ifp, base64_decode( $data[ 1 ] ) );
  fclose( $ifp );

  $baseurl = $baseurl."_.".$type;
  // RELEASE POOL
  echo json_encode( array("url" => $baseurl) );

} else {
  echo json_encode( array("dev" => "@lexcasa", "copyright" => "lexart labs - 2019") );
}

?>